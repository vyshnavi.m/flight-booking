package models

import "time"

const Host = "localhost"
const Port = 5432
const User = "postgres"
const Password = "shiny"
const Dbname = "my_db"

type Userinfo struct {
	Emailid  string `json:"emailid"`
	Password string `json:"password"`
	Username string `json:"username"`
}

type Passenger struct {
	ID      string    `json:"id"`
	Name    string    `json:"name"`
	Src     string    `json:"src"`
	Dest    string    `json:"dest"`
	Airline string    `json:"airline"`
	Phnum   string    `json:"phnum"`
	Seatnum string    `json:"seatnum"`
	Date    string    `json:"date"`
	Arr     string    `json:"arr"`
	Dept    string    `json:"dept"`
	Time    time.Time `json:"time"`
	Emailid string    `json:"emailid"`
}

type Flight struct {
	Flight_id int    `json:"flight_id"`
	Src       string `json:"src"`
	Dest      string `json:"dest"`
	Arr       string `json:"arr"`
	Dept      string `json:"dept"`
	Date      string `json:"date"`
	Airline   string `json:"airline"`
	Price     int    `json:"price"`
}
