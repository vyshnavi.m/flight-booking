package cache

import (
	"encoding/json"
	"time"

	"context"

	"../models"
	"github.com/go-redis/redis"
)

func getClient() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
}

func Set(key string, user models.Userinfo) {
	client := getClient()

	json, err := json.Marshal(user)
	if err != nil {
		panic(err)
	}
	ctx := context.Background()

	client.Set(ctx, key, json, 3600*time.Minute)
}

func Get(key string) *models.Userinfo {
	client := getClient()
	ctx := context.Background()
	val, err := client.Get(ctx, key).Result()
	if err != nil {
		return nil
	}

	// passenger := models.Passenger{}
	var user models.Userinfo
	err = json.Unmarshal([]byte(val), &user)
	if err != nil {
		panic(err)
	}

	return &user
}
