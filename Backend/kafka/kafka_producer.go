package kafka

import (
	"context"
	"encoding/json"
	"fmt"

	"../models"

	"github.com/segmentio/kafka-go"
)

func Produce(ctc context.Context, passenger models.Passenger) {

	w := kafka.NewWriter(kafka.WriterConfig{
		Brokers: []string{"localhost:9092"},
		Topic:   "flight-topic",
	})

	json, err := json.Marshal(passenger)
	if err != nil {
		fmt.Println("Producer json Marshall :", err)
	}

	// for {
	err = w.WriteMessages(ctc, kafka.Message{
		Key:   []byte(passenger.ID),
		Value: []byte(json),
	})

	if err != nil {
		fmt.Println("Producer kakfa :", err)
	}

	fmt.Println("Writes")

	//time.Sleep(time.Second)
	// }
}
