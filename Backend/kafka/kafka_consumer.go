package kafka

import (
	"context"
	"encoding/json"
	"fmt"

	"../models"

	"../mail"
	"github.com/segmentio/kafka-go"
)

func Consume(ctx context.Context) {
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{"localhost:9092"},
		Topic:   "flight-topic",
		GroupID: "g2",
	})

	// for {
	// the `ReadMessage` method blocks until we receive the next event
	msg, err := r.ReadMessage(ctx)
	if err != nil {
		panic("could not read message " + err.Error())
	}
	// after receiving the message, log its value
	var passenger models.Passenger
	e := json.Unmarshal(msg.Value, &passenger)
	if e != nil {
		fmt.Println("Consumer json Marshall :", err)
	}

	message := "To: " + passenger.Emailid + "\r\n" +
		"Subject: Booking Confirmation on Book My Trip on " + passenger.Date + "(" + passenger.Time.String() + ")\r\n" +
		"\r\n" +
		"Dear,\n   Thank you for using Book My Trip for booking a flight ticket. Your booking details are indicated below\n\n\nPassenger Name: " + passenger.Name + "\n\nFrom : " + passenger.Src + "\t\tTo: " + passenger.Dest + "\n\nAirline: " + passenger.Airline + "\n\nSeat Number :" + passenger.Seatnum
	mail.SendMail(passenger.Emailid, message)
	// }
}
