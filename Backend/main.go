package main

import (
	"fmt"
	"log"
	"net/http"

	"./db"
	"./handler"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {

	DB := db.Init()
	h := handler.New(DB)
	router := mux.NewRouter()
	headers := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization", "Origin"})
	methods := handlers.AllowedMethods([]string{"POST", "GET", "DELETE"})
	origins := handlers.AllowedOrigins([]string{"*"})

	router.HandleFunc("/passengers/{emailid}", h.GetAllData).Methods(http.MethodGet)
	// router.HandleFunc("/books/{id}", h.GetBook).Methods(http.MethodGet)
	router.HandleFunc("/passengers", h.SendData).Methods(http.MethodPost)
	// router.HandleFunc("/books/{id}", h.UpdateBook).Methods(http.MethodPut)
	router.HandleFunc("/passengers/{id}/{emailid}", h.DeleteData).Methods(http.MethodDelete)
	router.HandleFunc("/flight/{src}/{dest}/{date}", h.SearchFlight).Methods(http.MethodGet)
	router.HandleFunc("/user/{emailid}", h.GetUser).Methods(http.MethodGet)
	router.HandleFunc("/user", h.SendUser).Methods(http.MethodPost)

	fmt.Println("Starting server on the port 8080...")
	log.Fatal(http.ListenAndServe(":8080", handlers.CORS(headers, methods, origins)(router)))
}
