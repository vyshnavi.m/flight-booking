package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	// "io/ioutil"
	// "log"
	"net/http"

	"../cache"
	"../kafka"
	"../mail"
	"../models"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

type handler struct {
	DB *gorm.DB
}

func New(db *gorm.DB) handler {
	return handler{db}
}

func GenerateUUID() uuid.UUID {
	id := uuid.New()

	return id
}

func (h handler) SendData(w http.ResponseWriter, r *http.Request) {
	// json.Unmarshal(body, &passenger)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers:", "Origin, Content-Type, X-Auth-Token, Authorization")
	w.Header().Set("Content-Type", "application/json")
	var passenger models.Passenger
	_ = json.NewDecoder(r.Body).Decode(&passenger)
	// fmt.Println(passenger.Name)

	// Append to the Passengers table
	passenger.ID = GenerateUUID().String()
	if result := h.DB.Create(&passenger); result.Error != nil {
		fmt.Println(result.Error)
	}
	json.NewEncoder(w).Encode(passenger)

	ctx := context.Background()
	go kafka.Produce(ctx, passenger)
}

func (h handler) GetAllData(w http.ResponseWriter, r *http.Request) {
	var passengers []models.Passenger
	vars := mux.Vars(r)

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers:", "Origin, Content-Type, X-Auth-Token, Authorization")
	w.Header().Set("Content-Type", "application/json")

	if result := h.DB.Order("time").Where("emailid = ?", vars["emailid"]).Find(&passengers); result.Error != nil {
		fmt.Println(result.Error)
	}
	json.NewEncoder(w).Encode(passengers)
}

func (h handler) DeleteData(w http.ResponseWriter, r *http.Request) {
	// Read the dynamic id parameter
	vars := mux.Vars(r)
	id := vars["id"]

	var passenger models.Passenger
	h.DB.Where("id = ?", id).Find(&passenger)
	h.DB.Delete(&passenger)

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers:", "Origin, Content-Type, X-Auth-Token, Authorization")
	w.Header().Set("Content-Type", "application/json")
	// w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode("Deleted")

	// fmt.Println(vars["emailid"])
	message := "To: " + passenger.Emailid + "\r\n" +
		"Subject: Ticket Cancellation Confirmation on Book My Trip on " + "(" + time.Now().String() + ")\r\n" +
		"\r\n" +
		"Dear,\n  Your ticket has been successfully canclled. Refund will be done in 2-3 working days.\n Your booking details are indicated below\n\n\nPassenger Name: " + passenger.Name + "\n\nFrom : " + passenger.Src + "(" + passenger.Arr + ")\t\tTo: " + passenger.Dest + "(" + passenger.Dept + ")\n\nAirline: " + passenger.Airline + "\n\nSeat Number :" + passenger.Seatnum + "\n\nDate : " + passenger.Date
	mail.SendMail(vars["emailid"], message)

}

func (h handler) SearchFlight(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	var flights []models.Flight

	if result := h.DB.Where("src = ? AND dest = ? AND date = ?", vars["src"], vars["dest"], vars["date"]).Find(&flights); result.Error != nil {
		fmt.Println(result.Error)
	}
	// fmt.Println("fed")
	fmt.Println(flights)

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers:", "Origin, Content-Type, X-Auth-Token, Authorization")
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(flights)
}

// Post User
func (h handler) SendUser(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers:", "Origin, Content-Type, X-Auth-Token, Authorization")
	w.Header().Set("Content-Type", "application/json")
	var userinfo models.Userinfo
	_ = json.NewDecoder(r.Body).Decode(&userinfo)

	if result := h.DB.Create(&userinfo); result.Error != nil {
		fmt.Println(result.Error)
	}

	json.NewEncoder(w).Encode("Created")
}

// Get User
func (h handler) GetUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers:", "Origin, Content-Type, X-Auth-Token, Authorization")
	w.Header().Set("Content-Type", "application/json")

	var users models.Userinfo
	user := cache.Get(vars["emailid"])

	if user == nil {
		if result := h.DB.Where("emailid = ?", vars["emailid"]).Find(&users); result.Error != nil {
			fmt.Println(result.Error)
		}

		json.NewEncoder(w).Encode(users)
		cache.Set(vars["emailid"], users)
	} else {
		json.NewEncoder(w).Encode(user)
	}

}
