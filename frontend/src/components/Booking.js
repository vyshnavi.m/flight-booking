import Flightform from "./Flightform";
import Flight from "./Flight";
import { useState } from "react";

const Booking = ({ flights, onSearch, onToggle, onBook}) => {

  
  return (
    <div className='booking'>
        <div className="booking-flight">
          <div style={{paddingLeft: '470px', paddingBottom: '10px'}}>
            <img src="https://www.easemytrip.com/images/flight-img/home-fl-icn.png" style={{float: 'left', marginTop: '5px'}}></img>
            <h2 style={{color: 'whitesmoke', fontWeight: 'bold', float: 'left', marginLeft: '10px'}}>Book a Flight</h2>
          </div>
          <Flightform onSearch={onSearch} />
        </div>
        <div>
          <Flight flights={flights} onToggle={onToggle} onBook={onBook}/>
        </div>
        <div style={{margin: '60px'}}></div>
    </div>
  );
}

export default Booking;
