import {useState} from 'react'

function Signup({ onSignup }) {
    const [emailid,setEmailid] = useState('')
    const [password,setPassword] = useState('')
    const [cnfrimpassword,setCnfrimpassword] = useState('')
    const [name,setName] = useState('')
    const [ showPassword,setShowPassword ] = useState(false)

    const onSubmit = (e) => {
        e.preventDefault()

        if(emailid && password && cnfrimpassword && name) {
            if(password==cnfrimpassword){
                onSignup(emailid,password,name)
            }
            else{
                alert('Password is not Matching')
            }
        }
        else{
            alert('Enter the Values')
        }

        setEmailid('')
        setPassword('')
        setCnfrimpassword('')
        setName('')
        setShowPassword('')
    }

  return (
    <div>
        <form onSubmit={onSubmit} style={{position: 'absolute', top: '70px'}}>
            <input type="email" value={emailid} placeholder='Enter Email' onChange={(e) => setEmailid(e.target.value) } style={{padding: '5px', width: '300px', marginLeft: '50px'}}/>
            <br></br>
            <input type={showPassword ? "text" : "password"} placeholder='Enter Password' value={password} onChange={(e) => setPassword(e.target.value)} style={{padding: '5px', width: '300px', marginLeft: '50px',  marginTop: '30px'}}/>
            <br></br>
            <input type={showPassword ? "text" : "password"} placeholder='Confirm Password' value={cnfrimpassword} onChange={(e) => setCnfrimpassword(e.target.value)} style={{padding: '5px', width: '300px', marginLeft: '50px',  marginTop: '30px'}}/>
            <br></br>
            <input type="text" placeholder='Enter Name' value={name} onChange={(e) => setName(e.target.value)} style={{padding: '5px', width: '300px', marginLeft: '50px',  marginTop: '30px'}}/>
            <br></br>
            <input type="checkbox" value={showPassword} onChange={(e) => setShowPassword(e.currentTarget.checked)} style={{padding: '5px', marginLeft: '50px', marginTop: '30px'}}/>
            <label>Show Password</label>
            <br></br>
            <input  type='submit' value='Sign Up' style={{padding: '5px', width: '300px', marginLeft: '50px', backgroundColor: 'darkblue', fontWeight:'bold', fontSize:'large', color:'white', marginTop: '50px', border: 'none', borderRadius: '5px'}}/>
        </form>
    </div>
  )
}

export default Signup