import React, { useState } from 'react'
import { BrowserRouter, Route, Link, Routes } from 'react-router-dom'
import Header from './Header'
import Login from './Login'
import Signup from './Signup'

function Loginpage({ onLogin, onSignup, to }) {

  const [login,setLogin] = useState(true)
  return (
    <BrowserRouter>
    <div style={{marginLeft: '560px'}}>
    <Header title='Book my Trip'/>
    </div>
    <div className='login-page'>
        <ul>
            <li>
                <Link to = "/" style={{position: 'absolute', left: '0px'}}><button style={{width: '200px', padding: '10px', fontSize: 'larger', backgroundColor:!login ? 'white' : 'darkblue', color:!login ? 'black' :'white', border: 'black solid 1px', fontWeight:'bold'}} onClick={() => setLogin(true)}>Sign In</button></Link>
            </li>
            <li>
                <Link to={to}  style={{position: 'absolute', left: '200px'}}><button style={{width: '200px', padding: '10px', fontSize:'larger', backgroundColor:!login? 'darkblue' : 'white', color:!login ? 'white':'black', border: 'black solid 1px', fontWeight: 'bold'}} onClick={() => setLogin(false)}>Sign Up</button></Link>
            </li>
        </ul>
    <Routes>
        <Route exact path='/' element={< Login onLogin={onLogin}/>}></Route>
        <Route exact path='/signup' element={< Signup onSignup={onSignup}/>}></Route>
    </Routes>
    </div>
    </BrowserRouter>
  )
}

export default Loginpage