import React from 'react';


const Viewbooking = ({ passengers, onDelete }) => {
  return (
    <>
        {passengers.map((passenger) => (
            <div className='passsenger-list' key={passenger.id} style={{marginTop: '30px', border: 'solid black 1px', position: 'relative', width: '1200px', marginLeft: '140px', borderRadius: '5px'}}>
                <h4 style={{marginLeft: '60px', padding: '10px'}}>Name : {passenger.name}</h4>
                <ul style={{paddingBottom: '10px'}}>
                    <li style={{marginLeft: '30px'}}>From: {passenger.src}<i>({passenger.arr})</i></li>
                    <li style={{marginLeft: '30px', position:'absolute', left:'250px'}}>To: {passenger.dest}<i>({passenger.dept})</i></li>
                    <li style={{marginLeft: '30px', position:'absolute', left:'450px'}}>Airline: {passenger.airline}</li>
                    <li style={{marginLeft: '30px', position:'absolute', right:'450px'}}>Seatnum: {passenger.seatnum}</li>
                    <li style={{marginLeft: '30px', position:'absolute', right:'250px'}}>Date: {passenger.date}</li>
                    <button className='btn' style={{marginLeft : '20px', position:'absolute', right:'100px', backgroundColor: 'lightcoral', border: 'none', padding: '5px', borderRadius:'2px', fontWeight:'bold' }} onClick={() => onDelete(passenger.id,passenger.emailid)}>Delete</button> 
                </ul>
            </div>
        ))}
    </>
    )
};


export default Viewbooking;
