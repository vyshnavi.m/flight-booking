import { useState } from 'react';
import Datepicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css'

const Flightform = ({ onSearch }) => {
  const [src, setSrc] = useState('')
  const [dest, setDest] = useState('')
  const [date, setDate] = useState(null)

  function getDate(s){
    return s.split(" ").slice(1,4).join(" ")
}

  const onSubmit = (e) => {
    e.preventDefault()
    let text = date.toString()
    let d = getDate(text)
    console.log(d)

    if(src && dest && date){
      onSearch(src,dest,d)
    }
    else {
      alert("Enter the Value")
    }
  }

  const onSwap = (e) => {
    e.preventDefault()
    let temp = dest
    setDest(src)
    setSrc(temp)
  }

  return (
    <div className="form">
        <form className='flight-form' onSubmit={onSubmit}>
        <div> 
            <label className="label" style={{position: 'absolute', top: '13px', left: '60px', color: 'lightslategray'}}>From</label>

            <input type='text' style={{position: 'absolute', top: '50px', left: '60px', border: 'none', borderBottom:'solid black 1px'}} value={src} onChange={(e) => setSrc(e.target.value)} />

            <button  style={{position: 'absolute', top: '32px', left: '280px', border: 'solid 1px black', padding: '0', borderRadius: '50%', cursor: 'pointer'}} onClick={onSwap}><img src={require("/home/rhcl5_5cg6222zwq/Documents/Intern/frontend/src/images/swap.png")} className='img' style={{width: '25px'}}></img></button>

            <label className="label" style={{position: 'absolute', top: '13px', left: '360px', color: 'lightslategray'}}>To</label>

            <input type='text' value={dest} style={{position: 'absolute', top: '50px', left: '360px', border: 'none', borderBottom:'solid black 1px'}} onChange={(e) => setDest(e.target.value)}/>

            <label className="label" style={{position: 'absolute', top: '13px', left: '600px', color: 'lightslategray'}}>Date</label>

            <div style={{position: 'absolute', top: '50px', left: '600px'}}>
              <Datepicker  selected={date} onChange={date => setDate(date)} dateFormat='dd/MM/yyyy' />
            </div>

            <input style={{position: 'absolute', top: '30px', left: '860px', backgroundColor: 'rgb(255, 109, 56)', padding: '10px 40px 10px 40px', fontSize: 'larger', border: 'none', fontWeight: 'bold', color: 'white', borderRadius: '4px'}}  type='submit' value='Search' className='btn'/>
        </div>
      </form>
    </div>
  )
}

export default Flightform;
