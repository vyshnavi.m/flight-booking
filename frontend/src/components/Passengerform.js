import { useState } from 'react';


const Passengerform = ({flight, onBook}) => {

  const [name,setName] = useState('')
  const [phnum,setPhnum] = useState('')
  const [seatnum,setSeatnum] = useState('')

  const onSubmit = (e) => {
    e.preventDefault()
    
    if( name != '' && phnum != '' && seatnum != ''){
      onBook(name,flight.src,flight.dest,flight.airline,phnum,seatnum,flight.date,flight.arr,flight.dept);
    }
    else{
      alert('Please Fill The empty Field')
    }
    
    setName('')
    setPhnum('')
    setSeatnum('')
  }

  return (
    <div style={{marginTop: '50px', position: 'relative'}}>
        <h4 style={{marginBottom: '10px', marginLeft: '51px'}} >Additional Details</h4>
        <form >
            <input type="text" placeholder='Enter Passenger Name' style={{marginLeft: '54px', padding:'5px'}} value={name} onChange={(e) => setName(e.target.value)} />

            <input type="text" placeholder='Enter Phone Number' style={{marginLeft: '20px', padding:'5px'}} value={phnum} onChange={(e) => setPhnum(e.target.value)} />

            <input type="text" placeholder='Enter Seat Number' style={{marginLeft: '20px', padding:'5px'}} value={seatnum} onChange={(e) => setSeatnum(e.target.value)} />

            <input type ="button" className='btn' style={{marginLeft: '10px', position: 'absolute', right: '80px', borderRadius:'3px', bottom: '4px', fontSize:'15px', color: 'white', backgroundColor: 'rgb(255, 109, 56)', border: 'none', fontWeight: 'bold', padding: '5px 10px 5px 10px'}} value="Book" onClick={onSubmit}/>
        </form>
    </div>
  );
};



export default Passengerform;
