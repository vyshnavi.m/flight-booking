import {useState} from 'react'

function Login({ onLogin, onToggle }) {
    const [emailid,setEmailid] = useState('')
    const [password,setPassword] = useState('')
    const [ showPassword,setShowPassword ] = useState(false)
    
    const onSubmit = (e) => {
        e.preventDefault()

        if(emailid && password) {
            onLogin(emailid,password)
        }
        else{
            alert('Enter the Value')
        }
    }

  return (
    <div className='login' style={{position: 'absolute', top: '100px'}}>
        <form onSubmit={onSubmit}>
            <input type="email" placeholder='Enter Email' value={emailid} onChange={(e) => setEmailid(e.target.value)} style={{padding: '5px', width: '300px', marginLeft: '50px'}}/>
            <br></br>
            <input type={showPassword ? "text" : "password"} placeholder='Enter Password' value={password} onChange={(e) => setPassword(e.target.value)} style={{padding: '5px', width: '300px', marginLeft: '50px', marginTop: '30px'}}/>
            {/* <input type="password" name="password" autocomplete="current-password" required="" id="id_password"> */}
            {/* <i class="far fa-eye" id="togglePassword" style="margin-left: -30px; cursor: pointer;"></i> */}
            <br></br>
            <input type="checkbox" value={showPassword} onChange={(e) => setShowPassword(e.currentTarget.checked)} style={{padding: '5px', marginLeft: '50px', marginTop: '30px'}}/>
            <label>Show Password</label>
            <br></br>
            <input  type='submit' value='Sign In' style={{padding: '5px', width: '300px', marginLeft: '50px', backgroundColor: 'darkblue', fontWeight:'bold', fontSize:'large', color:'white', marginTop: '50px', border: 'none', borderRadius: '5px'}}/>
        </form>
    </div>
  )
}

export default Login