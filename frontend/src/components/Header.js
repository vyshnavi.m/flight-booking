import React from 'react';

const Header = ({title}) => {
  return (
    <header className='header'>
        <img src = {require("/home/rhcl5_5cg6222zwq/Documents/Intern/frontend/src/logo.jpg")} alt='Logo' width='80px' className='img' style={{float: 'left'}}></img>
        <h1 style={{float: 'left', marginTop: '25px', color: 'darkblue'}}>{title}</h1>
    </header>
  );
}

export default Header;
