// import { useState } from 'react';
import Passengerform from './Passengerform';



const Flight = ({ flights, onToggle, onBook}) => {

  return (
    <>
        {/* {flights.length && <h2 style={{padding: '20px', textAlign: 'center', color: 'darkblue'}}>Flights from {flights[0].src} to {flights[0].dest} on {flights[0].date}</h2>} */}
        {flights.map((flight) => (
            <div className='flight-data' key={flight.flight_id} style={{position: 'relative', padding: flight.toggle? '40px 40px 30px 40px' : '40px 40px 50px 40px', backgroundColor: 'white'}}>
                <ul>
                  <li className='flight-list' style={{position: 'absolute', left: '100px', top: '35px'}}>{flight.airline}</li>

                  <li className='flight-list' style={{position: 'absolute', left: '300px', top: '27px', fontSize: '25px'}}>{flight.arr}</li>

                  <li style={{position: 'absolute', left: '304px', top: '60px', color: '#4a4a4a', fontSize: 'small'}}>{flight.src}</li>

                  <li style={{position:'absolute', left: '400px', top:'60px'}}><hr style={{width: '100px', borderTop: 'solid 3px green'}}></hr></li>

                  <li className='flight-list' style={{position: 'absolute', left: '550px', top: '27px', fontSize: '25px'}}>{flight.dept}</li>

                  <li style={{position: 'absolute', left: '550px', top: '60px', color: '#4a4a4a', fontSize: 'small'}}>{flight.dest}</li>

                  <li className='flight-list' style={{position: 'absolute', left:'700px', top: '35px', fontWeight: 'bold'}}>{flight.price}</li>
                  
                  
                  {!flight.toggle &&
                    <li><a onClick={() => {onToggle(flight.flight_id)}} style={{position: 'absolute', right: '150px', color:'green', textDecoration:'underline', cursor:'pointer', top: '35px'}}>Book a Ticket</a></li> }

                  {flight.toggle && <li><a onClick={() => {onToggle(flight.flight_id)}} style={{position: 'absolute', right: '150px', color:'red', textDecoration:'underline', cursor:'pointer', top: '35px'}}>Close</a></li>}
                </ul>
                {flight.toggle &&  < Passengerform flight={flight} onBook={onBook}/>}
            </div>
        ))} 
    </>
  )
}

export default Flight;
