import './App.css';
import React from 'react';
import { BrowserRouter, Route, Link, Routes } from 'react-router-dom'
import { useState, useEffect } from 'react';
import Header from './components/Header';
import Booking from './components/Booking';
import axios from 'axios';
import Viewbooking from './components/Viewbooking';
import Loginpage from './components/Loginpage';


const App = () => {

  const [showFlight,setShowFlight] = useState(false)

  const [ flights, setFlights ] = useState([]);

  const [ passengers, setPassenger ] = useState([])
  const [ user, setUser ] = useState([])
  const [ showLogin, setShowLogin ] = useState(true)


  // Toggle the Passenger Form
  const togglePassenger = (id) => {
    const p = flights.map(flight => flight.flight_id==id ? {...flight, toggle: false}: {...flight, toggle: false})
    setFlights(p)
    const newPassenger = flights.map(flight => flight.flight_id==id ? {...flight, toggle: !flight.toggle} : flight)
    setFlights(newPassenger)
  }

  // Search the flights
  const searchFlights = (src,dest,date) => {
    console.log(src);
    console.log(dest);
    console.log(date);
    axios
      .get("http://localhost:8080/flight/" + src +"/" + dest+ "/" + date)
      .then(res => {
        console.log(res.data)
        setFlights(res.data)
      })
      .catch(err => console.error(err))
  }

  // Get the data
  useEffect(() => {
    axios
    .get("http://localhost:8080/passengers/" + user.emailid)
    .then(res => {setPassenger(res.data)})
    .catch(err => console.error(err))
  })

  // Post Passengers
  const sendPassenger = (name,src,dest,airline,phnum,seatnum,date,arr,dept) => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
      'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token'
    }
    
    axios.post('http://localhost:8080/passengers',{
      name : name,
      src: src,
      dest: dest,
      airline: airline,
      phnum: phnum,
      seatnum: seatnum,
      date: date,
      arr: arr,
      dept, dept,
      time: new Date(),
      emailid : user.emailid
    } ,headers)
    .then(res => alert("Successfully Ticket Booked"))
    .catch(err => console.error(err))
  }

  // Delete Passenger
  const DeletePassenger = (id, emailid) => {
    axios.delete('http://localhost:8080/passengers/'+id + '/' +emailid)
      .then(res => alert("Successfully Deleted"))
      .catch(err => console.error(err))
  }


  const toggleLogin = () => {
    setShowLogin(!showLogin)
  }

  // Get User
  const GetUser = (emailid,password) => {
    axios.get('http://localhost:8080/user/'+emailid)
      .then(res => {console.log(res); 
                  setUser(res.data)
                  if(res.data.password == password){
                    toggleLogin()
                  }
                  else {
                    alert('Enter the Correct Password')
                  }
                })
      .catch(err => console.error(err))
    // console.log(user.password)
    // console.log(password)
  }

  // Send User
  const SendUser = (emailid,password,username) => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
      'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token'
    }
    axios.post('http://localhost:8080/user',{
          emailid: emailid,
          password: password,
          username: username
        },headers)
          .then(res => { alert("Account Created") })
          .catch(err => console.error(err))
  }


  if(showLogin) {
    return (<Loginpage onLogin={GetUser} onSignup={SendUser} to='/signup'/>)
  }

  return (
    <BrowserRouter>
    <div className="container">
    <Header title="Book my Trip"/>
    <ul className='list'>
      <li className='main-list'>
        <Link to="/" className='link' style={{color: 'darkblue', fontSize: 'larger', marginLeft: '30px'}}>Book a Ticket</Link>
      </li>
      <li className='main-list'>
        <Link className='link' to='/viewbooking' style={{color: 'darkblue', fontSize: 'larger', marginLeft: '30px'}}>View Booking</Link>
      </li>
    </ul>
    <Routes>
      <Route exact path='/' element={< Booking flights={flights} onSearch={searchFlights} onToggle={togglePassenger} onBook={sendPassenger}/>}></Route>
      <Route exact path='/viewbooking' element={<Viewbooking passengers={passengers} onDelete={DeletePassenger}/>}></Route>
      {/* <Route exact path='/deletebooking' element={<Deletebooking passengers={passengers} />}></Route> */}
    </Routes>
    </div>
    </BrowserRouter>
  );
}

export default App;
