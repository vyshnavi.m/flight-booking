package kafka

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"../mail"
	"github.com/segmentio/kafka-go"
)

type Passenger struct {
	ID      string    `json:"id"`
	Name    string    `json:"name"`
	Src     string    `json:"src"`
	Dest    string    `json:"dest"`
	Airline string    `json:"airline"`
	Phnum   string    `json:"phnum"`
	Seatnum string    `json:"seatnum"`
	Date    string    `json:"date"`
	Arr     string    `json:"arr"`
	Dept    string    `json:"dept"`
	Time    time.Time `json:"time"`
	Emailid string    `json:"emailid"`
}

func Consume(ctx context.Context) {
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{"localhost:9092"},
		Topic:   "flight-topic",
		GroupID: "g2",
	})

	for {
		// the `ReadMessage` method blocks until we receive the next event
		msg, err := r.ReadMessage(ctx)
		if err != nil {
			panic("could not read message " + err.Error())
		}
		// after receiving the message, log its value
		var passenger Passenger
		e := json.Unmarshal(msg.Value, &passenger)
		if e != nil {
			fmt.Println("Consumer json Marshall :", err)
		}

		message := "To: " + passenger.Emailid + "\r\n" +
			"Subject: Booking Confirmation on Book My Trip on " + "(" + time.Now().String() + ")\r\n" +
			"\r\n" +
			"Dear,\n   Thank you for using Book My Trip for booking a flight ticket. Your booking details are indicated below\n\n\nPassenger Name: " + passenger.Name + "\n\nFrom : " + passenger.Src + "(" + passenger.Arr + ")\t\tTo: " + passenger.Dest + "(" + passenger.Dept + ")\n\nAirline: " + passenger.Airline + "\n\nSeat Number :" + passenger.Seatnum + "\n\nDate : " + passenger.Date
		mail.SendMail(passenger.Emailid, message)
	}
}
