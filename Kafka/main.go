package main

import (
	"./kafka"

	"context"
	"fmt"
)

func main() {
	ctx := context.Background()
	kafka.Consume(ctx)

	fmt.Println("Kafka is Started...")

	// time.Sleep(30 * time.Second)
}
